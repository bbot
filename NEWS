Version 0.17.0

  * Support for auxiliary machines. See the bbot manual for details.

  * Support for bbot.sys-install:config.install.root variable in worker.

Version 0.16.0

  * New bpkg.bindist.*, bbot.sys-install.*, bbot.install.ldconfig steps.

  * Support for build artifact upload (bbot.upload step).

  * Support for controller URL priorities, build job interrupts, CPU boosting.

  * Support for *-package-config package manifest value functionality.

  * Support for the `none` interactive CI breakpoint.

  * Support for aarch64 in the environment scripts.

  * New --install option in the bootstrap scripts.

  * New --{bootstrap,build}-startup agent options.

Version 0.15.0

  * Support for MSVC 17 (2022) in bootstrap and environment batch files.

  * Packages are now configured with config.<pkg>.develop=false to force
    skeleton load.

Version 0.14.0

  * New worker script with the target/host configuration split awareness.

  * Support for interactive builds.

  * Support for soft and hard rebuilds.

  * Re-distribution phase for checked out packages.

  * Emscripten environment script.

  * Compression of result manifests uploaded by worker.

  * Truncation of large operation result logs.

Version 0.13.0

  * Build logs are now UTF-8-sanitized by the worker.

  * Support for test-exclude task manifest value.

  * Bootstrap timeout has been increased from 30 to 60 minutes.

Version 0.12.0

  * Support for testing build system modules.

  * Support for tests, examples, and benchmarks as separate packages.

  * Support for passing the environment name as part of the task manifest.

  * Add bootstrap and environment batch files for Clang targeting MSVC.

  * Various improvements to environment scripts/batch files:

    - Pass the compiler mode options as part of config.{c,cxx}.

    - Update the MSVC and Clang environments to support running without the
      development command prompt.

  * Improve printing multi-line diagnostics to systemd journal.

  * Make the unexpected build machine exit a warning instead of an error.

Version 0.11.0

  * Increase default build timeout from 30 to 60 minutes.

Version 0.10.0

  * MSVC 16 bootstrap and environment batch scripts.

Version 0.9.0

  * Support for running multiple instances of bbot-agent for the same
    toolchain.

    This allows us to perform multiple bootstraps/builds in parallel. Note
    that each machine can only be used by a single instance so it doesn't make
    sense to have more instances than machines.

  * Use the build system's 'info' meta-operation to determine if the package
    project/subprojects can be tested.

  * Ability to specify an alternative bridge interface in bbot-agent.

  * Use QMP (QEMU Machine Protocol) events as a machine log file.

Version 0.8.0

  * Add 'repository-type' value to task manifest and rename 'repository' value
    to 'repository-url'.

  * Make communication with QEMU monitor more robust.

  * Disable audio output with QEMU_AUDIO_DRV=none.

Version 0.7.0

  * Assign "step id" to each step in "worker script". This is used to support
    step-specific command options. See bbot manual for details.

  * Support for controller request retries (--request-retries) and connection
    timeout (--connect-timeout) in bbot agent.

  * Increase default bootstrap timeout to 20 minutes.

Version 0.6.0

  * Support for installation testing (install, test installed, and uninstall
    operations). See the bbot manual for details.

  * Support for bbot agent authentication with controllers.

  * Support for warning detection regex in task manifests.

  * Multiple virtual machine management and communication robustness
    improvements.

Version 0.5.0

  * First public release.
