#!/bin/sh

# file      : etc/bootstrap/bbot-bootstrap-linux.sh
# license   : MIT; see accompanying LICENSE file

if ! jobs="$(nproc)"; then
  jobs=1
fi

# Enable core dumps (into /tmp by default) for postmortem debugging.
#
cd /tmp
ulimit -c unlimited

# Note: bootstrap script options must come before build.sh options/arguments.
#
"$(dirname "$0")/bbot-bootstrap.sh" --make make --jobs "$jobs" "$@"

# If success, wait a bit and shutdown. Otherwise leave the machine running
# for investigation.
#
if test "$?" -eq 0; then
  sleep 5
  sudo shutdown -h now
fi
