#!/bin/sh

# file      : etc/bootstrap/bbot-bootstrap.sh
# license   : MIT; see accompanying LICENSE file

usage="Usage: $0 [<options>] [<build-options>]"

set -e # Exit on errors.

diag ()
{
  echo "$*" 1>&2
}

error ()
{
  diag "$*"
  exit 1
}

# Note that this function will execute a command with arguments that contain
# spaces but it will not print them as quoted (and neither does set -x).
#
run ()
{
  diag "+ $@"
  "$@"
  if test "$?" -ne "0"; then
    exit 1
  fi
}

# Defaults that can be changed via command line.
#
install=
build=/tmp
environments="$HOME/environments"
make=
jobs=

# Parse options.
#
# Note that if --install is specified, any necessary --private/--sudo options
# will need to be specified as <build-options>.
#
while test $# -ne 0; do
  case $1 in
     --install)
      shift
      if test $# -eq 0; then
	error "missing install directory after --install"
      fi
      install="$1"
      shift
      ;;
    --build)
      shift
      if test $# -eq 0; then
	error "missing build directory after --build"
      fi
      build="$1"
      shift
      ;;
    --environments)
      shift
      if test $# -eq 0; then
	error "missing environments directory after --environments"
      fi
      environments="$1"
      shift
      ;;
    --make)
      shift
      if test $# -eq 0; then
	error "missing make program after --make"
      fi
      make="$1"
      shift
      ;;
    --jobs)
      shift
      if test $# -eq 0; then
	error "missing jobs number after --jobs"
      fi
      jobs="$1"
      shift
      ;;
    *)
      break
      ;;
  esac
done

# Defaults that can be changed for testing or further customization.
#
# Note: build_options is array-like (expanded unquoted).
#
tftp="196.254.111.222"
verbose=3
timeout=600
build_options=

#install="/tmp/bbot-install"
#tftp="127.0.0.1:55123"

if test -n "$install"; then
  build_options="$build_options --install-dir $install"
else
  install="/usr/local"
fi

# If make was specified, add it to build_options.
#
if test -n "$make"; then
  build_options="$build_options --make $make"

  if test -n "$jobs"; then
    build_options="$build_options --make -j$jobs"
  fi
fi

PATH="$install/bin:$PATH"
export PATH

# If we already have the bbot worker, assume we are bootstrapped.
#
if bbot-worker --version >/dev/null 2>&1; then
  exec bbot-worker --startup --build "$build" --environments "$environments" \
       --tftp-host "$tftp" --verbose "$verbose"
fi

# Bootstrap the toolchain and then build bbot.
#
run rm -rf "$build/bootstrap"
run mkdir -p "$build/bootstrap"
run cd "$build/bootstrap"

# We could be running on a new network which may take some time to setup.
# And if we start before that happens, we will be hanging forever.
#
while true; do
  diag "+ curl -s -S -O --connect-timeout 5 --max-time 60 tftp://$tftp/build2-toolchain.tar.xz"
  if      curl -s -S -O --connect-timeout 5 --max-time 60 "tftp://$tftp/build2-toolchain.tar.xz"; then
    break
  fi
done
run tar -xf build2-toolchain.tar.xz
run rm build2-toolchain.tar.xz

run curl -s -S -O "tftp://$tftp/trust"
trust="$(cat trust)"
run rm trust

# Bootstrap and install the toolchain (by default into /usr/local using sudo).
#
bstrap="$(echo build2-toolchain-*)"
run cd "$bstrap"
run ./build.sh --timeout "$timeout" --trust "$trust" $build_options "$@"
run cd ..
run rm -r "$bstrap"

# Build and install the bbot worker.
#
config="$(echo build2-toolchain-*)"
run cd "$config"
run bpkg --fetch-timeout "$timeout" build --yes bbot
run bpkg install bbot
run cd ..
run rm -r "$config"

# Finish off by uploading the bootstrap result manifest produced by the bbot
# worker.
#
run bbot-worker --bootstrap >bootstrap.manifest
run curl -s -S --upload-file bootstrap.manifest "tftp://$tftp/bootstrap.manifest"
