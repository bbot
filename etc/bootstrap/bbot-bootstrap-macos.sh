#!/bin/sh

# file      : etc/bootstrap/bbot-bootstrap-macos.sh
# license   : MIT; see accompanying LICENSE file

# open -a Terminal -n -F ~/bbot-bootstrap-macos.sh

cxx=clang++

if ! jobs="$(sysctl -n hw.ncpu)"; then
  jobs=1
fi

# Enable core dumps (into /cores/) for postmortem debugging.
#
ulimit -c unlimited

# Note: bootstrap script options must come before build.sh options/arguments.
#
"$(dirname "$0")/bbot-bootstrap.sh" \
--build /tmp \
--environments "$HOME/environments" \
--make make \
--jobs "$jobs" \
"$cxx" \
"$@"

# If success, wait a bit and shutdown. Otherwise leave the machine running
# for investigation.
#
if test "$?" -eq 0; then
  sleep 5
  sudo shutdown -h now
fi
