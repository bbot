#!/bin/sh

# file      : etc/bootstrap/bbot-bootstrap-freebsd.sh
# license   : MIT; see accompanying LICENSE file

# Note: install GNU make with 'pkg install gmake'.
#

if ! jobs="$(sysctl -n hw.ncpu)"; then
  jobs=1
fi

# Enable core dumps (into /tmp by default) for postmortem debugging. These
# are normally enabled by default but check also:
#
# sysctl -a | grep kern.coredump
#
cd /tmp
ulimit -c unlimited

# Note: bootstrap script options must come before build.sh options/arguments.
#
"$(dirname "$0")/bbot-bootstrap.sh" --make gmake --jobs "$jobs" "$@"

# If success, wait a bit and shutdown. Otherwise leave the machine running
# for investigation.
#
if test "$?" -eq 0; then
  sleep 5
  sudo shutdown -p now
fi
