@echo off

rem file      : etc/bootstrap/bbot-bootstrap-mingw.bat
rem license   : MIT; see accompanying LICENSE file

setlocal EnableExtensions EnableDelayedExpansion

goto start

@rem Remove a directory recursively. Noop for non-existent directory.
@rem
@rem Note that the underlying rmdir call periodically fails with the
@rem 'directory not empty' diagnostics, in which case we make up to 100
@rem removal attempts.
@rem
:rmdir_S

  @if not exist %1\ goto :eof

  @setlocal EnableDelayedExpansion

  @set /A "i=0"

  :rmdir_S_try

  @rem Note that rmdir doesn't properly set errorlevel, so we check for the
  @rem directory existence to detect the failure.
  @rem
  rmdir /S /Q %1\

  @if exist %1\ (
    if %i% neq 99 (
      set /A "i=%i%+1"
      goto rmdir_S_try
    )

    echo error: unable to remove %1\
    endlocal
    exit /b 1
  )

  @endlocal
@goto :eof

:start

set "BUILD=C:\tmp"
set "INSTALL=C:\build2"
set "BOOTSTRAP=C:\bootstrap"
set "ENVIRONMENTS=C:\environments"

set "TFTP=196.254.111.222"
rem set "TFTP=10.1.0.1:55123"
set "VERBOSE=3"
set "TIMEOUT=600"

rem If we already have the bbot worker, assume we are bootstrapped.
rem
if exist %INSTALL%\bin\bbot-worker.exe (
  set "PATH=%INSTALL%\bin;%PATH%"
  bbot-worker.exe --startup --build %BUILD% --environments %ENVIRONMENTS%^
 --tftp-host %TFTP% --verbose %VERBOSE%
  goto end
)

rem Use bootstrap tools.
rem
set "PATH=%BOOTSTRAP%\bin;%PATH%"

rem Show the steps we are performing.
rem
@echo on

@rem
@rem Bootstrap the toolchain and then build bbot.
@rem
@call :rmdir_S %BUILD%\bootstrap
@if errorlevel 1 goto error

mkdir %BUILD%\bootstrap
@if errorlevel 1 goto error

@call :rmdir_S %INSTALL%
@if errorlevel 1 goto error

cd %BUILD%\bootstrap

@rem
@rem Get the baseutils.
@rem
@rem We could be running on a new network which may take Windows some time
@rem to digest. And if we start before that happens, we will be hanging
@rem forever.
@rem

:restart
curl -s -S -O --connect-timeout 5 --max-time 60^
  "tftp://%tftp%/build2-baseutils-x86_64-windows.zip"
@if errorlevel 1 goto restart

unzip -q build2-baseutils-x86_64-windows.zip
@if errorlevel 1 goto error

del build2-baseutils-x86_64-windows.zip
@if errorlevel 1 goto error

move build2-baseutils-*-x86_64-windows %INSTALL%
@if errorlevel 1 goto error


@rem
@rem Get the mingw toolchain.
@rem
curl -s -S -O "tftp://%tftp%/build2-mingw-x86_64-windows.tar.xz"
@if errorlevel 1 goto error

xz -d build2-mingw-x86_64-windows.tar.xz
@if errorlevel 1 goto error

tar -xf build2-mingw-x86_64-windows.tar^
  --one-top-level=%INSTALL% --strip-components=1
@if errorlevel 1 goto error

del build2-mingw-x86_64-windows.tar
@if errorlevel 1 goto error


@rem
@rem Get the toolchain.
@rem
curl -s -S -O "tftp://%tftp%/build2-toolchain.tar.xz"
@if errorlevel 1 goto error

xz -d build2-toolchain.tar.xz
@if errorlevel 1 goto error

tar -xf build2-toolchain.tar
@if errorlevel 1 goto error

del build2-toolchain.tar
@if errorlevel 1 goto error


@rem
@rem Get the repository certificate fingerprint.
@rem
curl -s -S -O "tftp://%tftp%/trust"
@if errorlevel 1 goto error

@set /P trust=<trust

del trust
@if errorlevel 1 goto error


@rem
@rem Switch to the bootstrapped utilities/toolchain.
@rem
@set "PATH=%INSTALL%\bin;%PATH%"


@rem
@rem Bootstrap and install the toolchain.
@rem
@for /D %%d in (build2-toolchain-*) do @set "bstrap=%%d"
cd %bstrap%

@rem Bootstrap in parallel using GNU make (which is part of baseutils).
@rem
@rem Note: executing in a separate cmd.exe to preserve the echo mode.
@rem
cmd /C build-mingw.bat --make mingw32-make --make -j%NUMBER_OF_PROCESSORS%^
  --timeout %TIMEOUT% --install-dir %INSTALL% --trust %trust% g++
@if errorlevel 1 goto error

cd ..
@call :rmdir_S %bstrap%
@if errorlevel 1 goto error


@rem
@rem Build and install the bbot worker.
@rem
@for /D %%d in (build2-toolchain-*) do set "config=%%d"
cd %config%

bpkg --fetch-timeout %TIMEOUT% build --yes bbot
@if errorlevel 1 goto error

bpkg install bbot
@if errorlevel 1 goto error

cd ..
@call :rmdir_S %config%
@if errorlevel 1 goto error

@rem
@rem Finish off by uploading the bootstrap result manifest produced by
@rem the bbot worker.
@rem
bbot-worker --bootstrap >bootstrap.manifest
@if errorlevel 1 goto error

curl -s -S --upload-file bootstrap.manifest "tftp://%tftp%/bootstrap.manifest"
@if errorlevel 1 goto error

shutdown /s /t 5
@if errorlevel 1 goto error

@echo off
goto end

:error
@echo off
endlocal
exit /b 1

:end
endlocal
