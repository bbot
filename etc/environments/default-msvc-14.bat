@echo off

rem file      : etc/environments/default-msvc-14.bat
rem license   : MIT; see accompanying LICENSE file

rem
rem Environment setup script for C/C++ compilation with Visual Studio 14.
rem

set "MSVC=C:\Program Files (x86)\Microsoft Visual Studio 14.0"
set "VCVARS=VC\vcvarsall.bat"

rem %1  - target
rem %2  - bbot executable (if absent, then run %COMSPEC%)
rem %3+ - bbot options

setlocal EnableExtensions EnableDelayedExpansion

rem Based on target determine what we are building.
rem
if "_%1_" == "_x86_64-microsoft-win32-msvc14.0_" (
  set "VCARCH=amd64"
) else (
  if "_%1_" == "_i386-microsoft-win32-msvc14.0_" (
    set "VCARCH=amd64_x86"
  ) else (
    echo error: unknown target %1
    goto error
  )
)

call "%MSVC%\%VCVARS%" %VCARCH%
if errorlevel 1 goto error

if not "_%2_" == "__" (
  %2 %3 %4 %5 %6 %7 %8 %9 cc config.c=cl config.cxx=cl
  if errorlevel 1 goto error
) else (
  echo config.c=cl
  echo config.cxx=cl
  %COMSPEC%
)

goto end

:error
@echo off
endlocal
exit /b 1

:end
endlocal
