@echo off

rem file      : etc/environments/default-clang.bat
rem license   : MIT; see accompanying LICENSE file

rem
rem Environment setup script for C/C++ compilation with Clang targeting
rem MSVC.
rem

rem NOTE: don't forget to adjust the target mode selection below.
rem
set "C=clang"
set "CXX=clang++"

rem If the MSVC and VCVARS variables are set, then set up the environment
rem via the MSVC command prompt rather than letting Clang find some default
rem (note that in the latter case, clang++ should be in PATH).
rem
rem Note also that MSVC_VER must always be set.

set "MSVC="

rem set "MSVC=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community"
rem set "MSVC_VER=14.1"

rem set "MSVC=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community"
rem set "MSVC_VER=14.2"

set "MSVC=C:\Program Files\Microsoft Visual Studio\2022\Community"
set "MSVC_VER=14.3"

set "VCVARS32=VC\Auxiliary\Build\vcvarsamd64_x86.bat"
set "VCVARS64=VC\Auxiliary\Build\vcvars64.bat"

rem %1  - target
rem %2  - bbot executable (if absent, then run %COMSPEC%)
rem %3+ - bbot options

setlocal EnableExtensions EnableDelayedExpansion

rem Based on target determine what we are building.
rem
set "VCVARS="
set "MODE="
if "_%1_" == "_x86_64-microsoft-win32-msvc%MSVC_VER%_" (
  set "VCVARS=%VCVARS64%"
  rem set "MODE=-m64"
) else (
  if "_%1_" == "_i686-microsoft-win32-msvc%MSVC_VER%_" (
    set "VCVARS=%VCVARS32%"
    set "MODE=-m32"
  ) else (
    if "_%1_" == "_i386-microsoft-win32-msvc%MSVC_VER%_" (
      set "VCVARS=%VCVARS32%"
      set "MODE=-m32"
    ) else (
      echo error: unknown target %1
      goto error
    )
  )
)

if not "_%MSVC%_" == "__" (
  if not "_%VCVARS%_" == "__" (
    call "%MSVC%\%VCVARS%"
    if errorlevel 1 goto error
  )
)

if not "_%2_" == "__" (
  %2 %3 %4 %5 %6 %7 %8 %9 cc "config.c=%C% %MODE%" "config.cxx=%CXX% %MODE%"
  if errorlevel 1 goto error
) else (
  echo config.c=%C% %MODE%
  echo config.cxx=%CXX% %MODE%
  %COMSPEC%
)

goto end

:error
@echo off
endlocal
exit /b 1

:end
endlocal
