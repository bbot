@echo off

rem file      : etc/environments/default-mingw.bat
rem license   : MIT; see accompanying LICENSE file

rem
rem Environment setup script for C/C++ compilation with MinGW GCC.
rem

rem NOTE: don't forget to adjust the target mode selection below.
rem
set "C=gcc"
set "CXX=g++"
set "MINGW=C:\build2\bin"

rem %1  - target
rem %2  - bbot executable (if absent, then run %COMSPEC%)
rem %3+ - bbot options

setlocal EnableExtensions EnableDelayedExpansion

set "PATH=$MINGW;%PATH%"

rem Based on target determine what we are building.
rem
set "MODE="
if "_%1_" == "_x86_64-w64-mingw32_" (
  rem set "MODE=-m64"
) else (
  if "_%1_" == "_i686-w64-mingw32_" (
    set "MODE=-m32"
  ) else (
    echo error: unknown target %1
    goto error
  )
)

if not "_%2_" == "__" (
  %2 %3 %4 %5 %6 %7 %8 %9 cc "config.c=%C% %MODE%" "config.cxx=%CXX% %MODE%"
  if errorlevel 1 goto error
) else (
  echo config.c=%C% %MODE%
  echo config.cxx=%CXX% %MODE%
  %COMSPEC%
)

goto end

:error
@echo off
endlocal
exit /b 1

:end
endlocal
