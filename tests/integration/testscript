# file      : tests/integration/testscript
# license   : MIT; see accompanying LICENSE file

# NOTE: to see the worker execution progress run the test with the
# --no-diag-buffer option.

#\
# Requirement:
#
# 1. tftpd-hpa installed (assumed in /usr/sbin/in.tftpd)
#
# 2. b, bpkg, curl executables in PATH.
#
# TFTP server (tftpd-hpa) setup: from the test out_base, run (sudo is required
# for --secure/chroot):
#

sudo /usr/sbin/in.tftpd \
  --foreground \
  --address 127.0.0.1:55123 \
  --user "$(whoami)" \
  --permissive \
  --create \
  --secure \
  --map-file tftp-map \
  "$(pwd)"

#\

machine = linux-gcc
target = x86_64-linux-gnu
environment =
c   = gcc
cxx = g++

# Where we get the task and what we do with the result can be configured
# independently:
#
# - We can poll a controller for a task by specifying its URL or we can
#   read a task manifest from a file (--fake-request).
#
# - We can send the result back to the controller or we can dump the result
#   to stdout (--dump-result).
#

#\
wait = 5 # Seconds.
controller = https://stage.build2.org/?build-task
#\

wait = 1 # Seconds.
controller = --fake-request ../task --dump-result

# Note that we also need to make sure that the installed package libraries are
# properly imported when configuring and running tests, and that the installed
# executables are runnable.
#
config = "bpkg.create:config.install.root=\"'$~/install'\" \
bpkg.configure.fetch:--fetch-timeout=60 \
bpkg.global.configure.build:--fetch-timeout=60 \
bpkg.create:config.bin.rpath=\"'$~/install/lib'\" \
config.cc.coptions=-Wall \
b.test-installed.configure:\"config.cc.loptions=-L'$~/install/lib'\" \
bpkg.test-separate-installed.create:\"config.cc.loptions=-L'$~/install/lib'\""

pkg = libhello
ver = 1.0.0+12
#rep_url = "https://git.build2.org/hello/libhello.git#1.0"
#rep_type = git
rep_url = https://stage.build2.org/1
rep_type = pkg
rfp = yes
#host='host: true'
#dependency_checksum = 'dependency-checksum: e6f10587696020674c260669f4e7000a0139df72467bff9770aea2f2b8b57ba0'

#package_config = 'package-config: { config.libhello.extras=true }+ libhello'
#package_config = 'package-config: +bbot.install.ldconfig:'
#package_config = 'package-config: -bpkg.install:'
#package_config = 'package-config: -b.test-installed.create:'
#\
package_config = "package-config:
\\
+bpkg.bindist.archive:--archive-build-meta=
bpkg.bindist.archive:config.install.relocatable=true

+bbot.bindist.upload:

bbot.sys-install.tar.extract:--directory=$~
bbot.sys-install.tar.extract:--strip-components=1

b.test-installed.configure:config.cc.loptions='-L$~/usr/local/lib'
bpkg.test-separate-installed.create:config.cc.loptions='-L$~/usr/local/lib'

bpkg.create:config.bin.rpath=[null]
\\"
config = "$config bpkg.create:config.install.root=\"'$~/usr/local'\""
+export LD_LIBRARY_PATH="$~/usr/local/lib:$getenv('LD_LIBRARY_PATH')"
#\
#\
sys_install_dir = [dir_path] $~/sys-install/
package_config = "package-config:
\\
+bpkg.bindist.archive:--archive-build-meta=
bpkg.bindist.archive:config.install.relocatable=true

+bbot.bindist.upload:

b.test-installed.configure:config.cc.loptions='-L$sys_install_dir/lib'
bpkg.test-separate-installed.create:config.cc.loptions='-L$sys_install_dir/lib'

bpkg.create:config.bin.rpath=[null]
\\"
config = "$config bpkg.create:config.install.root=\"'$~/usr/local'\" \
bbot.sys-install:config.install.root=\"'$sys_install_dir'\" \
bbot.sys-install.tar.extract:--directory=\"$sys_install_dir\" \
bbot.sys-install.tar.extract:--strip-components=3"
+export LD_LIBRARY_PATH="$sys_install_dir/lib:$getenv('LD_LIBRARY_PATH')"
#\
#\
package_config = 'package-config:
\
+bpkg.bindist.fedora:
+bbot.bindist.upload:
bpkg.create:config.bin.rpath=[null]
\'
#\
#\
package_config = 'package-config:\
bpkg.configure.fetch:--fetch-timeout=120 -bpkg.install:
config.libhello.develop=true
sys:libuuid-c++ --sys-install --sys-no-stub --sys-yes
\
'
#\

#\
pkg = hello
ver = 1.0.0+8
rep_url = "https://git.build2.org/hello/hello.git"
rep_type = git
rfp = yes
#\

#\
pkg = libstudxml
ver = 1.1.0-b.10
rep_url = https://stage.build2.org/1
rep_type = pkg
rfp = yes
#\

# To make sure that the test-installed phase succeeds use the build2 driver
# installed into a writable directory, for example, ~/install/bin.
#
#\
pkg = libbuild2-hello
ver = 0.2.0
rep_url = "https://github.com/build2/libbuild2-hello.git#master"
rep_type = git
#rep_url = https://stage.build2.org/1
#rep_type = pkg
rfp = yes
tests = "tests: * libbuild2-hello-tests == $ver"
host = 'host: true'
#\
#package_config = 'package-config: -bpkg.install:'
#\
package_config = 'package-config:
\
+bpkg.bindist.fedora:
bpkg.module.create:config.bin.rpath=[null]
\'
#\

# Use the build2 driver installed into ~/install/bin (see above).
#
#\
pkg = libbuild2-kconfig
ver = 0.3.0
rep_url = "https://github.com/build2/libbuild2-kconfig.git#master"
rep_type = git
#ver = 0.1.0-a.0.20200910053253.a71aa3f3938b
#rep_url = https://stage.build2.org/1
#rep_type = pkg
rfp = yes
requires = 'requires: bootstrap'
tests = "tests: * libbuild2-kconfig-tests == $ver
examples: * kconfig-hello == $ver"
host = 'host: true'
#\
#\
package_config = 'package-config:
\
{ config.libbuild2_kconfig-tests.extras=true }+ libbuild2-kconfig-tests
\'
#\
#package_config = 'package-config: config.libbuild2_kconfig.develop=true'
#package_config = 'package-config: -bpkg.install:'
#\
package_config = "package-config:
\\
+bpkg.bindist.archive:--archive-build-meta=
bbot.sys-install.tar.extract:--directory=$~
bbot.sys-install.tar.extract:--strip-components=1

+bbot.bindist.upload:

bpkg.create:config.bin.rpath=[null]
\\"
config = "$config bpkg.create:config.install.root=\"'$~/usr/local'\""
+export LD_LIBRARY_PATH="$~/usr/local/lib:$getenv('LD_LIBRARY_PATH')"
#\
#\
package_config = 'package-config:
\
+bpkg.bindist.fedora:
bpkg.module.create:config.bin.rpath=[null]
\'
#\
#dependency_checksum = 'dependency-checksum: 72ae02bed9a05aaf022147297a99b84d63b712e15d05cc073551da39003e87e8'

# Use the build2 driver installed into ~/install/bin (see above).
#
#\
pkg = libbuild2-autoconf
ver = 0.3.0
rep_url = "https://github.com/build2/libbuild2-autoconf.git#master"
rep_type = git
rfp = yes
tests = "tests: * libbuild2-autoconf-tests == $ver"
host = 'host: true'
#\

#\
pkg = curl
ver = 8.8.0+1
rep_url = https://pkg.cppget.org/1/stable
rep_type = pkg
rfp = yes
#\

#\
pkg = bpkg
ver = 0.16.0-a.0.20230201123204.d956e69e8b55
rep_url = https://stage.build2.org/1
rep_type = pkg
rfp = yes
package_config = 'package-config:
\
config.bpkg.tests.remote=true
?libodb-sqlite +{ config.libodb_sqlite.develop=true }
?cli +{ config.cli.develop=true }
?sys:libsqlite3 --sys-install
\'
#\

#\
pkg = cli
ver = 1.2.0
rep_url = "https://git.codesynthesis.com/cli/cli.git#master"
rep_type = git
#rep_url = https://stage.build2.org/1
#rep_type = pkg
rfp = yes
requires='requires: host'
tests="tests: * cli-tests == $ver
examples: * cli-examples == $ver"
host='host: true'
#\
#package_config = "package-config: -b.test-installed.create: -bpkg.test-separate-installed.create:"
#\
package_config = "package-config:
\\
{ config.cli.extras=true }+ cli
{ config.cli_tests.extras=true }+ cli-tests
{ config.cli_examples.extras=true }+ cli-examples
\\"
#\
#\
package_config = "package-config:
\\
+bpkg.bindist.archive:--archive-build-meta=
bbot.sys-install.tar.extract:--directory=$~
bbot.sys-install.tar.extract:--strip-components=1

+bbot.bindist.upload:

bpkg.create:config.bin.rpath=[null]
\\"
config = "$config bpkg.create:config.install.root=\"'$~/usr/local'\""
+export LD_LIBRARY_PATH="$~/usr/local/lib:$getenv('LD_LIBRARY_PATH')"
#\
#\
package_config = 'package-config:
\
+bpkg.bindist.fedora:

+bbot.bindist.upload:

bpkg.create:config.bin.rpath=[null]
\'
#\
#package_config = 'package-config: ?libcutl +{ config.libcutl.develop=true }'
#package_config = 'package-config: -bpkg.install:'

#\
pkg = libxsd
ver = 4.2.1-a.0.20231121133547.a821131cf17a
rep_url = "https://git.codesynthesis.com/xsd/xsd.git#master"
rep_type = git
#rep_url = https://stage.build2.org/1
#rep_type = pkg
rfp = yes
tests="tests: libxsd-tests == $ver"
#\

#\
pkg = xsd
ver = 4.2.1-a.0.20231121133547.a821131cf17a
rep_url = "https://git.codesynthesis.com/xsd/xsd.git#master"
rep_type = git
#rep_url = https://queue.stage.build2.org/1
#rep_type = pkg
rfp = yes
requires='requires: host'
tests="tests: * xsd-tests == $ver
examples: * xsd-examples == $ver"
host='host: true'
#\
#\
package_config = "package-config:
\\
?sys:libxerces-c --sys-install --sys-yes
{ config.xsd_tests.extras=true }+ xsd-tests
+bpkg.bindist.archive:--archive-build-meta=
bbot.sys-install.tar.extract:--directory=$~
bbot.sys-install.tar.extract:--strip-components=1
bpkg.create:config.bin.rpath=[null]
\\"
config = "$config bpkg.create:config.install.root=\"'$~/usr/local'\""
+export LD_LIBRARY_PATH="$~/usr/local/lib:$getenv('LD_LIBRARY_PATH')"
#\
#\
package_config = 'package-config:
\
?sys:libxerces-c --sys-install --sys-yes
+bpkg.bindist.fedora:
bpkg.create:config.bin.rpath=[null]
\'
#\
#package_config = 'package-config:
# \
#?libxerces-c +{ config.libxerces_c.network=true }
#?libcurl/7.76.0
#?sys:libz/*
# \'

#dependency_checksum = 'dependency-checksum: 40a0ad4546d836a3afc83a9e7da22f2b5d224af4e62996d88f7103eaee23e9e1'

#\
pkg = libxerces-c
ver = 3.2.4
rep_url = https://pkg.cppget.org/1/stable
rep_type = pkg
rfp = yes
package_config = 'package-config:
\
config.libxerces_c.network=true
"?libcurl ~7.76.0"
sys:libz/*
-bpkg.update:
\'
#\

#\
pkg = odb
ver = 2.5.0
rep_url = "https://git.codesynthesis.com/odb/odb.git#master"
rep_type = git
rfp = yes
requires='requires: host'
host='host: true'
#\
#\
package_config = 'package-config:
\
+bpkg.bindist.archive:
+bpkg.bindist.archive.post:
+bbot.bindist.upload:

# Target config.
#
bpkg.bindist.archive:--archive-install-root=/tmp/local
bbot.sys-install.tar.extract:--directory=/tmp/local
bbot.sys-install.tar.extract:--strip-components=3

bpkg.bindist.archive:--recursive=auto
bpkg.bindist.archive:--archive-lang-impl=cc=
bpkg.bindist.archive:config.install.relocatable=true

bpkg.bindist.archive.post:build/bindist-archive-post.bx

bpkg.create:config.bin.lib=static
bpkg.create:config.cc.coptions=-fPIC

b.create:config.cc.coptions="-Wall -Werror -O2 -fstack-protector-strong "
\'
config = "$config bbot.sys-install:config.install.root=/tmp/local"
#\
#\
package_config = 'package-config:
\
+bpkg.bindist.archive:
+bbot.bindist.upload:
bpkg.bindist.archive:--recursive=auto

bpkg.bindist.archive:--archive-install-root=/tmp
bbot.sys-install:config.install.root=/tmp/install
bbot.sys-install.tar.extract:--directory=/tmp/install
bbot.sys-install.tar.extract:--strip-components=1

bpkg.create:config.bin.lib=static
bpkg.create:config.cc.coptions=-fPIC

b.create:config.cc.coptions="-Wall -Werror -O2 -fstack-protector-strong "

b.create:config.cxx.std=c++11
\'
#\

#\
pkg = libcmark-gfm-extensions
ver = 0.29.0-a.4
rep_url = https://stage.build2.org/1
rep_type = pkg
rfp = yes
host='host: true'
#\

#\
pkg = non-existing
ver = 0.1.0
rep_url = https://stage.build2.org/1
rep_type = pkg
rfp = yes
#\

#\
pkg = fmt
ver = 8.1.1
rep_url = "https://github.com/build2-packaging/fmt.git"
#rep_url = "git+file:/tmp/fmt#master"
rep_type = git
rfp = yes
#\

#\
pkg = libodb-sqlite
ver = 2.5.0-b.28.20241030072332.1de3f3f222ba
rep_url = "https://git.codesynthesis.com/var/scm/odb/odb.git#win-bin"
rep_type = git
rfp = yes
tests="tests: odb-tests == $ver"' ? (!$defined(config.odb_tests.database)) config.odb_tests.database=sqlite
tests: odb-examples == '"$ver"' ? (!$defined(config.odb_examples.database)) config.odb_examples.database=sqlite'

package_config = 'package-config:
\
# Configure odb-{tests,examples} packages dependency libodb as system on the
# bpkg.test-separate-installed.configure.build step, since it is
# already installed as a dependency of libodb-sqlite.
#
{ --config-uuid=00000000-0000-0000-0000-000000000005 }+ ?sys:libodb/*
\'
#\
#\
package_config = 'package-config:
\
{ config.odb_tests.multi_database=true }+ odb-tests
\'
#\

#\
pkg = libodb-pgsql
ver = 2.5.0-b.26.20240131175206.1c7f67f47770
rep_url = "https://git.codesynthesis.com/var/scm/odb/odb.git#master"
rep_type = git
rfp = yes
tests="tests: odb-tests == $ver"' ? (!$defined(config.odb_tests.database)) config.odb_tests.database=pgsql
tests: odb-examples == '"$ver"' ? (!$defined(config.odb_examples.database)) config.odb_examples.database=pgsql'
#\
#\
package_config = 'package-config:
\
{ config.odb_tests.database="sqlite pgsql" }+ odb-tests
\'
#\

#\
pkg = odb-tests
ver = 2.5.0-b.26.20240131175206.1c7f67f47770
rep_url = "https://git.codesynthesis.com/var/scm/odb/odb.git#master"
rep_type = git
rfp = yes
#\
#\
package_config = 'package-config:
\
config.odb_tests.database="sqlite pgsql"
\'
#\

#\
pkg = libodb-oracle
ver = 2.5.0-b.26.20240201133448.3fa01c83a095
rep_url = "https://git.codesynthesis.com/var/scm/odb/odb.git#master"
rep_type = git
rfp = yes
package_config = 'package-config:
\
config.cc.poptions+=-I/usr/include/oracle/12.2/client64 config.cc.loptions+=-L/usr/lib/oracle/12.2/client64/lib
\'
#\

#\
pkg = libodb-qt
ver = 2.5.0-b.26.20240201180613.633ad7ccad39
rep_url = "https://git.codesynthesis.com/var/scm/odb/odb.git#master"
rep_type = git
rfp = yes
#\

#\
pkg = libQt6Widgets
ver = 6.7.3
rep_url = "https://github.com/build2-packaging/Qt6.git#master"
rep_type = git
rfp = yes
tests="tests: libQt6WidgetsTests == $ver"
#\

#interactive="interactive: b.test-installed.configure"
#interactive="interactive: warning"
#\
aux_env = 'auxiliary-environment:
\
# x86_64-linux_debian_12-postgresql_15
#
DATABASE_HOST=10.0.213.126
DATABASE_PORT=5432
DATABASE_USER=test
DATABASE_NAME=test
\
'
#\

+cat <<"EOI" >=task
  : 1
  name: $pkg
  version: $ver
  repository-url: $rep_url
  repository-type: $rep_type
  trust: $rfp
  $requires
  $tests
  machine: $machine
  target: $target
  $aux_env
  target-config: $config
  $package_config
  $interactive
  $host
  worker-checksum: 1
  $dependency_checksum
  EOI

+if ("$environment" != "")
  echo "environment: $environment" >+task
end

#
#
tftp = 127.0.0.1:55123

a = $0
+sed -e 's/-agent$/-worker/' <"$0" | set w

: agent
:
{
  cat <<"EOI" >=machine-header;
    : 1
    id: $machine-1.0
    name: $machine
    summary: The $machine fake machine
    EOI
  $a --verbose 3 --tftp $~ --fake-machine machine-header $controller \
     &build/*** >| 2>|
}

: worker
:
{
  env = ("$environment" != "" ? "$environment" : "default")

  cat <<"EOI" >=$env;
  #!/bin/sh

  t="\$1"
  shift

  exec "\$@" cc config.c=$c config.cxx=$cxx
  EOI
  chmod ugo+x $env;
  sleep $wait;
  $w --verbose 3 --startup --tftp-host $tftp --environments $~ \
    &?build/*** &?build-host/*** &?build-module/*** &?build-install/*** \
    &?build-installed/*** &?build-installed-bpkg/*** \
    &?build-installed-bpkg-module/*** &?build-installed-bpkg-host/*** \
    &?dist/*** &?redist/*** &?bindist/*** \
    &?dist-install/*** &?redist-install/*** \
    &?dist-installed/*** &?redist-installed/*** \
    &?../usr/*** &?upload/*** &?upload.tar \
    &?../sys-install/*** \
    &task.manifest <| 2>|;

  sudo rm -rf ../sys-install/
}
