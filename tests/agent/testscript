# file      : tests/agent/testscript
# license   : MIT; see accompanying LICENSE file

# The /build/machines directory should be on a btrfs filesystem and have the
# following layout/contents:
#
# /build/machines/
# └── default/
#     └── linux-gcc/
#         ├── linux-gcc-1.0/
#         │    └── manifest
#         └── linux-gcc-1.1/
#               └── manifest
#
# Notes:
#
#  - The test must be run serially (@@ TODO: serial directive)

test.options = --verbose 3

cp = [cmdline] $src_base/btrfs-cpdir -f /build/machines.orig /build/machines
rm = [cmdline] $src_base/btrfs-rmdir /build/machines

: dump-machines
:
{
  test.options += --dump-machines --fake-bootstrap

  m = /build/machines/default/linux-gcc

  +$cp

  : no-current-machine-symlink
  :
  $* --toolchain-id 123 2>>"EOE"
    trace: enumerate_machines: skipping $m/: no subvolume link
    EOE

  ln --no-target-directory -s linux-gcc-1.0 $m/linux-gcc-1

  : bootstrap
  :
  $* --toolchain-id 123 >>EOO 2>>~"%EOE%d"
    : 1
    id: linux-gcc-1.0
    name: linux-gcc
    summary: Linux with GCC
    EOO
    %trace: enumerate_machines: btrfs subvolume snapshot $m/linux-gcc-1.0 $m/linux-gcc-default-\\.+%
    trace: enumerate_machines: bootstrapping $m/linux-gcc-default/
    trace: enumerate_machines: bootstrapped linux-gcc
    EOE

  ln --no-target-directory -sf linux-gcc-1.1 $m/linux-gcc-1

  : re-bootstrap-machine
  :
  $* --toolchain-id 123 >>EOO 2>>~"%EOE%d"
    : 1
    id: linux-gcc-1.1
    name: linux-gcc
    summary: Linux with GCC
    EOO
    %trace: enumerate_machines: btrfs subvolume snapshot $m/linux-gcc-1.1 $m/linux-gcc-default-\\.+%
    trace: enumerate_machines: re-bootstrapping $m/linux-gcc-default/: new machine
    trace: enumerate_machines: btrfs property set -ts $m/linux-gcc-default ro false
    trace: enumerate_machines: btrfs subvolume delete $m/linux-gcc-default
    trace: enumerate_machines: bootstrapped linux-gcc
    EOE

  : re-bootstrap-toolchain
  :
  $* --toolchain-id 124 >>EOO 2>>~"%EOE%d"
    : 1
    id: linux-gcc-1.1
    name: linux-gcc
    summary: Linux with GCC
    EOO
    %trace: enumerate_machines: btrfs subvolume snapshot $m/linux-gcc-1.1 $m/linux-gcc-default-\\.+%
    trace: enumerate_machines: re-bootstrapping $m/linux-gcc-default/: new toolchain
    trace: enumerate_machines: btrfs property set -ts $m/linux-gcc-default ro false
    trace: enumerate_machines: btrfs subvolume delete $m/linux-gcc-default
    trace: enumerate_machines: bootstrapped linux-gcc
    EOE

  sed -i -e 's/^(bbot-version):.*/\1: 0.0.1/' $m/linux-gcc-default/manifest

  : re-bootstrap-bbot
  :
  $* --toolchain-id 124 >>EOO 2>>~"%EOE%d"
    : 1
    id: linux-gcc-1.1
    name: linux-gcc
    summary: Linux with GCC
    EOO
    %trace: enumerate_machines: btrfs subvolume snapshot $m/linux-gcc-1.1 $m/linux-gcc-default-\\.+%
    trace: enumerate_machines: re-bootstrapping $m/linux-gcc-default/: new bbot
    trace: enumerate_machines: btrfs property set -ts $m/linux-gcc-default ro false
    trace: enumerate_machines: btrfs subvolume delete $m/linux-gcc-default
    trace: enumerate_machines: bootstrapped linux-gcc
    EOE

  sed -i -e 's/^(bbot-version):.*/\1: 999.999.999/' $m/linux-gcc-default/manifest

  : re-bootstrap-bbot-newer
  :
  $* --toolchain-id 124 2>>~"%EOE%d"
    %trace: enumerate_machines: btrfs subvolume snapshot $m/linux-gcc-1.1 $m/linux-gcc-default-\\.+%
    trace: enumerate_machines: ignoring $m/linux-gcc-default/: old bbot
    %trace: enumerate_machines: btrfs subvolume delete $m/linux-gcc-default-\\.+%
    EOE

  -$rm
}

: dump-result
:
{
  test.options += --fake-bootstrap --fake-build --fake-request -

  m = /build/machines/default/linux-gcc

  +$cp
  +ln --no-target-directory -s linux-gcc-1.0 $m/linux-gcc-1
  +$* --dump-machines --verbose 1 >- # Bootstrap.

  : no-machine
  :
  $* --dump-task <<EOI 2>>~"%EOE%d"
    : 1
    name: foo
    version: 1.2.3
    repository-url: https://example.org/1/
    repository-type: pkg
    machine: windows-msvc
    target: x86_64-microsoft-win32-msvc14.1
    EOI
    %trace: enumerate_machines:\\.*%*
    error: task from http://example.org for unknown machine windows-msvc
    EOE

  : result
  :
  $* --dump-result <<EOI >>EOO 2>>~"%EOE%d"
    : 1
    name: foo
    version: 1.2.3
    repository-url: https://example.org/1/
    repository-type: pkg
    machine: linux-gcc
    target: x86_64-linux-gnu
    EOI
    : 1
    name: foo
    version: 1.2.3
    status: abort
    EOO
    %trace: enumerate_machines:\\.*%*
    EOE

  -$rm
}
