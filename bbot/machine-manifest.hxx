// file      : bbot/machine-manifest.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef BBOT_MACHINE_MANIFEST_HXX
#define BBOT_MACHINE_MANIFEST_HXX

#include <map>

#include <libbutl/manifest-forward.hxx>

#include <libbbot/manifest.hxx> // machine_header

#include <bbot/types.hxx>
#include <bbot/utility.hxx>

#include <bbot/bootstrap-manifest.hxx>

namespace bbot
{
  // Machine type.
  //
  enum class machine_type {kvm, nspawn};

  string
  to_string (machine_type);

  machine_type
  to_machine_type (const string&); // Throws invalid_argument.

  // Machine.
  //
  class machine_manifest: public machine_header_manifest
  {
  public:
    machine_type type;
    optional<string> mac;      // Required in bootstrapped machine manifest.
    optional<strings> options; // Note: could be quoted.
    strings changes;

    strings
    unquoted_options () const; // Return empty if absent.

    machine_manifest (string i,
                      string n,
                      string s,
                      machine_type t,
                      optional<string> m,
                      optional<strings> o,
                      strings c,
                      optional<machine_role> r,
                      optional<uint64_t> rmn,
                      optional<uint64_t> rmx)
        : machine_header_manifest (move (i),
                                   move (n),
                                   move (s),
                                   r,
                                   rmn,
                                   rmx),
          type (t),
          mac (move (m)),
          options (move (o)),
          changes (move (c)) {}

  public:
    machine_manifest () = default;
    machine_manifest (butl::manifest_parser&, bool ignore_unknown = false);
    machine_manifest (butl::manifest_parser&,
                      butl::manifest_name_value start,
                      bool ignore_unknown = false);

    void
    serialize (butl::manifest_serializer&) const;
  };

  // Toolchain.
  //
  class toolchain_manifest
  {
  public:

    // Toolchain id (SHAXXX).
    //
    string id;

    explicit
    toolchain_manifest (string i): id (i) {}

  public:
    toolchain_manifest () = default;
    toolchain_manifest (butl::manifest_parser&, bool ignore_unknown = false);
    toolchain_manifest (butl::manifest_parser&,
                        butl::manifest_name_value start,
                        bool ignore_unknown = false);

    void
    serialize (butl::manifest_serializer&) const;
  };

  // The manifest stored in <name>-<toolchain>/ consists of the machine
  // manifest (original) and, if this a build machine, toolchain manifest and
  // bootstrap manifest.
  //
  class bootstrapped_machine_manifest
  {
  public:
    machine_manifest   machine;
    toolchain_manifest toolchain;
    bootstrap_manifest bootstrap;

    bootstrapped_machine_manifest (machine_manifest m,
                                   toolchain_manifest t,
                                   bootstrap_manifest b)
        : machine (move (m)), toolchain (move (t)), bootstrap (move (b)) {}

  public:
    bootstrapped_machine_manifest () = default;
    bootstrapped_machine_manifest (butl::manifest_parser&,
                                   bool ignore_unknown = false);

    void
    serialize (butl::manifest_serializer&) const;
  };
}

#endif // BBOT_MACHINE_MANIFEST_HXX
