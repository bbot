// file      : bbot/agent/tftp.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef BBOT_AGENT_TFTP_HXX
#define BBOT_AGENT_TFTP_HXX

#include <bbot/types.hxx>
#include <bbot/utility.hxx>

namespace bbot
{
  // A TFTP server "wrapper" over tftpd-hpa.
  //
  // In a nutshell, we are pretending to be inetd and when a request arrives,
  // spawn tftpd-hpa to handle it.
  //
  class tftp_server
  {
  public:
    // The map argument specifies the path mapping rules, one per line (see
    // the tftpd-hpa --map-file|-m option for details). If port is 0, then
    // it is automatically assigned.
    //
    tftp_server (const string& map, uint16_t port);

    // Return the assigned port.
    //
    uint16_t
    port () const;

    // Wait for a TFTP request for up to the specified number of seconds. If
    // increment is not 0, then wait in the specified increments (i.e., wait
    // for up to that number of seconds; useful when one needs to also
    // periodically check for something else). Update the timeout value as
    // well as return true if a request was served and false otherwise.
    //
    bool
    serve (size_t& seconds, size_t increment = 0);

  private:
    auto_fd fd_;
    auto_rmfile map_;
  };
}

#endif // BBOT_AGENT_TFTP_HXX
