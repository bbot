// file      : bbot/agent/http-service.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef BBOT_AGENT_HTTP_SERVICE_HXX
#define BBOT_AGENT_HTTP_SERVICE_HXX

#include <libbutl/manifest-types.hxx>

#include <bbot/types.hxx>
#include <bbot/utility.hxx>

#include <bbot/agent/agent-options.hxx>

// NOTE: this implementation is inspired by the bdep's http_service::post()
// function. The key difference is the result::error member which is used
// to return rather than fail on upload errors.

namespace bbot
{
  namespace http_service
  {
    // If type is file, then the value is a path to be uploaded. If type is
    // file_text, then the value is a file content to be uploaded.
    //
    struct parameter
    {
      enum {text, file, file_text} type;
      string name;
      string value;
    };
    using parameters = vector<parameter>;

    struct result
    {
      // If error is present, then it contains the description of why the
      // upload failed. In this case message contains additional information.
      //
      optional<string> error;
      string           message;
      optional<string> reference;

      // Does not include status, message, or reference.
      //
      vector<butl::manifest_name_value> body;
    };

    // Submit text parameters and/or upload files to an HTTP service via the
    // POST method. Use the multipart/form-data content type if any files are
    // uploaded and application/x-www-form-urlencoded otherwise.
    //
    // Note: currently only one file_text parameter can be specified.
    //
    // Return the response manifest message and reference (if present, see
    // below) and the rest of the manifest values, if any. If unable to
    // retrieve the response manifest, the message can also be set to the
    // first line of the plain text error description or, as a last resort,
    // constructed from the HTTP status code and reason phrase. Issue
    // diagnostics and throw failed if something is wrong with the setup
    // (unable to execute curl, etc).
    //
    // Note that the HTTP service is expected to respond with the result
    // manifest that starts with the 'status' (HTTP status code) and 'message'
    // (diagnostics message) values optionally followed by 'reference' and
    // then other manifest values.
    //
    result
    post (const agent_options&, const string& url, const parameters&);
  }
}

#endif // BBOT_AGENT_HTTP_SERVICE_HXX
