// file      : bbot/agent/agent.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef BBOT_AGENT_AGENT_HXX
#define BBOT_AGENT_AGENT_HXX

#include <sys/types.h> // uid_t

#include <bbot/types.hxx>
#include <bbot/utility.hxx>

#include <bbot/agent/agent-options.hxx>

namespace bbot
{
  extern agent_options ops;

  extern const string bs_prot; // Bootstrap protocol version.

  extern string           tc_name; // Toolchain name.
  extern uint16_t         tc_num;  // Toolchain number.
  extern standard_version tc_ver;  // Toolchain version.
  extern string           tc_id;   // Toolchain id.

  extern uint16_t inst; // Instance number (1-based).

  extern string hname; // Our host name.
  extern string hip;   // Our IP address.
  extern uid_t  uid;   // Our effective user id.
  extern string uname; // Our effective user name.

  extern uint16_t offset; // Agent offset (10-9990; used for ports).

  // Random number generator (currently not MT-safe and limited to RAND_MAX).
  //
  size_t
  genrand ();

  template <typename T>
  inline T
  genrand () {return static_cast<T> (genrand ());}

  // Return the IPv4 address of an interface.
  //
  string
  iface_addr (const string&);
}

#endif // BBOT_AGENT_AGENT_HXX
