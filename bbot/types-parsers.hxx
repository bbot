// file      : bbot/types-parsers.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

// CLI parsers, included into the generated source files.
//

#ifndef BBOT_TYPES_PARSERS_HXX
#define BBOT_TYPES_PARSERS_HXX

#include <libbbot/manifest.hxx>

#include <bbot/types.hxx>

namespace bbot
{
  namespace cli
  {
    class scanner;

    template <typename T>
    struct parser;

    template <>
    struct parser<path>
    {
      static void
      parse (path&, bool&, scanner&);
    };

    template <>
    struct parser<dir_path>
    {
      static void
      parse (dir_path&, bool&, scanner&);
    };

    template <>
    struct parser<standard_version>
    {
      static void
      parse (standard_version&, bool&, scanner&);
    };

    template <>
    struct parser<interactive_mode>
    {
      static void
      parse (interactive_mode&, bool&, scanner&);
    };
  }
}

#endif // BBOT_TYPES_PARSERS_HXX
