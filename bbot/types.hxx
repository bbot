// file      : bbot/types.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef BBOT_TYPES_HXX
#define BBOT_TYPES_HXX

#include <vector>
#include <string>
#include <memory>        // unique_ptr, shared_ptr
#include <utility>       // pair
#include <cstddef>       // size_t, nullptr_t
#include <cstdint>       // uint{8,16,32,64}_t
#include <istream>
#include <ostream>
#include <functional>    // function, reference_wrapper

#include <ios>           // ios_base::failure
#include <exception>     // exception
#include <stdexcept>     // logic_error, invalid_argument, runtime_error
#include <system_error>

#include <libbutl/path.hxx>
#include <libbutl/optional.hxx>
#include <libbutl/fdstream.hxx>
#include <libbutl/lz4-stream.hxx>
#include <libbutl/vector-view.hxx>
#include <libbutl/small-vector.hxx>
#include <libbutl/standard-version.hxx>

namespace bbot
{
  // Commonly-used types.
  //
  using std::uint8_t;
  using std::uint16_t;
  using std::uint32_t;
  using std::uint64_t;

  using std::size_t;
  using std::nullptr_t;

  using std::pair;
  using std::string;
  using std::function;
  using std::reference_wrapper;

  using std::unique_ptr;
  using std::shared_ptr;
  using std::weak_ptr;

  using std::vector;

  using strings = vector<string>;
  using cstrings = vector<const char*>;

  using std::istream;
  using std::ostream;

  // Exceptions. While <exception> is included, there is no using for
  // std::exception -- use qualified.
  //
  using std::logic_error;
  using std::invalid_argument;
  using std::runtime_error;
  using std::system_error;
  using std::generic_category;
  using io_error = std::ios_base::failure;

  // <libbutl/optional.hxx>
  //
  using butl::optional;
  using butl::nullopt;

  // <libbutl/fdstream.hxx>
  //
  using butl::auto_fd;
  using butl::ifdstream;
  using butl::ofdstream;

  // <libbutl/lz4-stream.hxx>
  //
  using olz4stream = butl::lz4::ostream;
  using ilz4stream = butl::lz4::istream;

  // <libbutl/vector-view.hxx>
  //
  using butl::vector_view;

  // <libbutl/small-vector.hxx>
  //
  using butl::small_vector;

  // <libbutl/path.hxx>
  //
  using butl::path;
  using butl::dir_path;
  using butl::basic_path;
  using butl::invalid_path;

  using paths = std::vector<path>;
  using dir_paths = std::vector<dir_path>;

  // <libbutl/standard-version.hxx>
  //
  using butl::standard_version;
}

// In order to be found (via ADL) these have to be either in std:: or in
// butl::. The latter is bad idea since libbutl includes the default
// implementation.
//
namespace std
{
  // Custom path printing (canonicalized, with trailing slash for directories).
  //
  inline ostream&
  operator<< (ostream& os, const ::butl::path& p)
  {
    string r (p.representation ());
    ::butl::path::traits_type::canonicalize (r);
    return os << r;
  }
}

#endif // BBOT_TYPES_HXX
