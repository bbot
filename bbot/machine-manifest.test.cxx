// file      : bbot/machine-manifest.test.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <ios>      // ios_base::failbit, ios_base::badbit
#include <iostream>

#include <libbutl/manifest-parser.hxx>
#include <libbutl/manifest-serializer.hxx>

#include <bbot/types.hxx>
#include <bbot/utility.hxx>

#include <bbot/machine-manifest.hxx>

#undef NDEBUG
#include <cassert>

using namespace std;
using namespace butl;
using namespace bbot;

// Usage: argv[0] (-m|-t|-bm)
//
// Read and parse manifest from STDIN and serialize it to STDOUT. The
// following options specify the manifest type.
//
// -m  parse machine manifest
// -t  parse toolchain manifest
// -bm parse bootstrapped machine manifest
//
int
main (int argc, char* argv[])
try
{
  assert (argc == 2);
  string opt (argv[1]);

  cin.exceptions  (ios_base::failbit | ios_base::badbit);
  cout.exceptions (ios_base::failbit | ios_base::badbit);

  manifest_parser     p (cin,  "stdin");
  manifest_serializer s (cout, "stdout");

  if (opt == "-m")
    machine_manifest (p).serialize (s);
  else if (opt == "-t")
    toolchain_manifest (p).serialize (s);
  else if (opt == "-bm")
    bootstrapped_machine_manifest (p).serialize (s);
  else
    assert (false);

  return 0;
}
catch (const manifest_parsing& e)
{
  cerr << e << endl;
  return 1;
}
catch (const manifest_serialization& e)
{
  cerr << e << endl;
  return 1;
}
