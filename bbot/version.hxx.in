// file      : bbot/version.hxx.in -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef BBOT_VERSION // Note: using the version macro itself.

// The numeric version format is AAAAABBBBBCCCCCDDDE where:
//
// AAAAA - major version number
// BBBBB - minor version number
// CCCCC - bugfix version number
// DDD   - alpha / beta (DDD + 500) version number
// E     - final (0) / snapshot (1)
//
// When DDDE is not 0, 1 is subtracted from AAAAABBBBBCCCCC. For example:
//
// Version      AAAAABBBBBCCCCCDDDE
//
// 0.1.0        0000000001000000000
// 0.1.2        0000000001000020000
// 1.2.3        0000100002000030000
// 2.2.0-a.1    0000200001999990010
// 3.0.0-b.2    0000299999999995020
// 2.2.0-a.1.z  0000200001999990011
//
#define BBOT_VERSION       $bbot.version.project_number$ULL
#define BBOT_VERSION_STR   "$bbot.version.project$"
#define BBOT_VERSION_ID    "$bbot.version.project_id$"

#define BBOT_VERSION_MAJOR $bbot.version.major$
#define BBOT_VERSION_MINOR $bbot.version.minor$
#define BBOT_VERSION_PATCH $bbot.version.patch$

#define BBOT_PRE_RELEASE   $bbot.version.pre_release$

#define BBOT_SNAPSHOT      $bbot.version.snapshot_sn$ULL
#define BBOT_SNAPSHOT_ID   "$bbot.version.snapshot_id$"

#include <libbutl/version.hxx>

$libbutl.check(LIBBUTL_VERSION, LIBBUTL_SNAPSHOT)$

#include <libbbot/version.hxx>

$libbbot.check(LIBBBOT_VERSION, LIBBBOT_SNAPSHOT)$

// User agent.
//
#if   defined(_WIN32)
# if defined(__MINGW32__)
#  define BBOT_OS "MinGW"
# else
#  define BBOT_OS "Windows"
# endif
#elif defined(__linux__)
#  define BBOT_OS "GNU/Linux"
#elif defined(__APPLE__)
#  define BBOT_OS "MacOS"
#elif defined(__CYGWIN__)
#  define BBOT_OS "Cygwin"
#elif defined(__FreeBSD__)
#  define BBOT_OS "FreeBSD"
#elif defined(__OpenBSD__)
#  define BBOT_OS "OpenBSD"
#elif defined(__NetBSD__)
#  define BBOT_OS "NetBSD"
#elif defined(__sun)
#  define BBOT_OS "Solaris"
#elif defined(__hpux)
#  define BBOT_OS "HP-UX"
#elif defined(_AIX)
#  define BBOT_OS "AIX"
#elif defined(__unix)
#  define BBOT_OS "Unix"
#elif defined(__posix)
#  define BBOT_OS "Posix"
#else
#  define BBOT_OS "Other"
#endif

#define BBOT_USER_AGENT                                             \
  "bbot/" BBOT_VERSION_ID " (" BBOT_OS "; +https://build2.org)"     \
  " libbbot/" LIBBBOT_VERSION_ID                                    \
  " libbutl/" LIBBUTL_VERSION_ID

#endif // BBOT_VERSION
