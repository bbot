// file      : bbot/types-parsers.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <bbot/types-parsers.hxx>

#include <bbot/common-options.hxx> // bbot::cli namespace

namespace bbot
{
  namespace cli
  {
    template <typename T>
    static void
    parse_path (T& x, scanner& s)
    {
      const char* o (s.next ());

      if (!s.more ())
        throw missing_value (o);

      const char* v (s.next ());

      try
      {
        x = T (v);

        if (x.empty ())
          throw invalid_value (o, v);
      }
      catch (const invalid_path&)
      {
        throw invalid_value (o, v);
      }
    }

    void parser<path>::
    parse (path& x, bool& xs, scanner& s)
    {
      xs = true;
      parse_path (x, s);
    }

    void parser<dir_path>::
    parse (dir_path& x, bool& xs, scanner& s)
    {
      xs = true;
      parse_path (x, s);
    }

    void parser<standard_version>::
    parse (standard_version& x, bool& xs, scanner& s)
    {
      xs = true;
      const char* o (s.next ());

      if (!s.more ())
        throw missing_value (o);

      const char* v (s.next ());

      try
      {
        x = standard_version (v);
      }
      catch (const invalid_argument& e)
      {
        throw invalid_value (o, v, e.what ());
      }
    }

    void parser<interactive_mode>::
    parse (interactive_mode& x, bool& xs, scanner& s)
    {
      xs = true;
      const char* o (s.next ());

      if (!s.more ())
        throw missing_value (o);

      const char* v (s.next ());

      try
      {
        x = to_interactive_mode (v);
      }
      catch (const invalid_argument&)
      {
        throw invalid_value (o, v);
      }
    }
  }
}
