// file      : bbot/bootstrap-manifest.test.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <ios>      // ios_base::failbit, ios_base::badbit
#include <iostream>

#include <libbutl/manifest-parser.hxx>
#include <libbutl/manifest-serializer.hxx>

#include <bbot/types.hxx>
#include <bbot/utility.hxx>

#include <bbot/bootstrap-manifest.hxx>

#undef NDEBUG
#include <cassert>

using namespace std;
using namespace butl;
using namespace bbot;

// Usage: argv[0]
//
// Read and parse bootstrap manifest from STDIN and serialize it to STDOUT.
//
int
main ()
try
{
  cin.exceptions  (ios_base::failbit | ios_base::badbit);
  cout.exceptions (ios_base::failbit | ios_base::badbit);

  manifest_parser     p (cin,  "stdin");
  manifest_serializer s (cout, "stdout");

  bootstrap_manifest (p).serialize (s);
  return 0;
}
catch (const manifest_parsing& e)
{
  cerr << e << endl;
  return 1;
}
catch (const manifest_serialization& e)
{
  cerr << e << endl;
  return 1;
}
