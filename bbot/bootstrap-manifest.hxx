// file      : bbot/bootstrap-manifest.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef BBOT_BOOTSTRAP_MANIFEST_HXX
#define BBOT_BOOTSTRAP_MANIFEST_HXX

#include <map>

#include <libbutl/manifest-forward.hxx>

#include <bbot/types.hxx>
#include <bbot/utility.hxx>

namespace bbot
{
  // Bootstrap result manifest. Uploaded by the worker to the agent's TFTP
  // server.
  //
  class bootstrap_manifest
  {
  public:

    // Map of packages to their (standard project) versions that were used
    // inside the bootstrapped machine. Used to make sure bbot agent/worker
    // use the same versions. For example:
    //
    // libbbot-version: 1.2.3
    // bbot-version:    1.2.4-a.0.1234.de2f
    //
    using versions_type = std::map<string, standard_version>;
    versions_type versions;

    explicit
    bootstrap_manifest (versions_type v)
        : versions (move (v)) {}

  public:
    bootstrap_manifest () = default;
    bootstrap_manifest (butl::manifest_parser&, bool ignore_unknown = false);
    bootstrap_manifest (butl::manifest_parser&,
                        butl::manifest_name_value start,
                        bool ignore_unknown = false);

    void
    serialize (butl::manifest_serializer&) const;
  };
}

#endif // BBOT_BOOTSTRAP_MANIFEST_HXX
